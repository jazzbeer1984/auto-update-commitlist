import sys
import getopt
from GerritAccess import GerritAccess
from UpdateDoc import UpdateDoc


def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h:u:a:b:", ["help", "url", "auth", "branch"])
    except getopt.GetoptError:
        #print msg
        print("for help use --help")
        return 2

    url = ''
    branch = ''
    username = ''
    password = ''

    # process options
    for o, a in opts:
        if o in ("-h", "--help"):
            print(__doc__)
            return 0
        elif o in ("-u", "--url"):
            url = a
        elif o in ("-a", "--auth"):
            attrs = a.split(':')
            if len(attrs) == 2:
                username = attrs[0]
                password = attrs[1]
        elif o in ("-b", "--branch"):
            branch = a

    if not url:
        print('URL must be set.')
        return 2
    if not branch:
        print('Branch must be set.')
        return 2
    if not username or not password:
        print('Username and Password must be set.')
        return 2

    gerrit = GerritAccess(url, username, password)
    cherrypick_gerrit_id_dict = gerrit.get_cherrypick_gerrit_ids(branch)
    if not cherrypick_gerrit_id_dict:
        print('Request Fail')
        return 2
    cherrypick_commit_id_dict = gerrit.get_cherrypick_commit_ids_by_gids(cherrypick_gerrit_id_dict)

    updateDoc = UpdateDoc()
    updateDoc.update_doc(UpdateDoc.SOURCE_DOC_NAME, UpdateDoc.DEST_DOC_NAME, UpdateDoc.SHEETS, cherrypick_commit_id_dict)

    return 0

if __name__== "__main__":
    sys.exit(main())
