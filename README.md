# Auto Update CommitList

使用 Gerrit API 自動搜尋目標專案上所有的 Commit，
將其 Commit Message 中 Cherry-pick URL 的資訊逐筆查詢出 Commit id，
再將此資訊更新之目標 CommitList 之 Excel 中。
執行結束後產生 NewCommitList.xlsx



## 本專案使用 pipenv 管理套件
    apt install python-pip
    pip install pipenv

## 須更新的 CommitList 範例
192_LC_Feature_New.xlsx

## CommitList 規則如下：
### 預設個分頁名稱：
'173 CommitList', '181 CommitList', '191 CommitList'

### 預設欄位名稱：
#### 'CommitID'：
將 Gerrit 上 192 Project 所有工單之 Cherry-pick URL 做比對
#### 'Status'：
有使用此張 Commit 的而尚未有 Merge 的，更新狀態為 Upload
有使用並 Merged 的更改為 Merge。
#### 'CommitID in 192'：
此欄位為 Option，會紀錄使用此張 Commit 的所有工單狀態

## Attributes：
* -u: Gerrit URL
* -b: Branch
* -a: Auth

## 執行
    pipenv run python Update.py -u [gerrit url] -b [branch] -a [username]:[password]
