import openpyxl
from openpyxl import Workbook
from openpyxl import load_workbook

class UpdateDoc:
    SOURCE_DOC_NAME = '192_LC_Feature_New.xlsx'
    DEST_DOC_NAME = 'NewCommitList.xlsx'
    SHEETS = ['173 CommitList', '181 CommitList', '191 CommitList']

    def update_doc(self, src_doc, dest_doc, sheets, cherrypick_commit_id_dict):
        wb_src = load_workbook(src_doc)
        for sheet_name in sheets: 
            ws_src = wb_src[sheet_name]

            #SRC
            commitId_column_index = 0
            cherrypick_column_index = 0
            status_column_index = 0
            new_commitId_column_index = 0

            for row in ws_src.rows:
                row_value = [cell.value for cell in row]
                if 'CommitID' in row_value:
                    commitId_column_index = row_value.index('CommitID')
                    if 'Need Cherry-pick?' in row_value:
                        cherrypick_column_index = row_value.index('Need Cherry-pick?')
                    if 'Status' in row_value:
                        status_column_index = row_value.index('Status')
                    if 'CommitID in 192' in row_value:
                        new_commitId_column_index = row_value.index('CommitID in 192')
                    break

            commitId_src_list = []
            for column in ws_src.columns:
                column_value = [cell.value for cell in column]
                if 'CommitID' in column_value:
                    commitId_src_list = column_value

            for cherrypick_cid, new_cids in cherrypick_commit_id_dict.items():
                if cherrypick_cid in commitId_src_list:
                    row_index = commitId_src_list.index(cherrypick_cid)
                    new_status = '' #'NotStart', 'Merged', 'Upload'
                    new_commit_comment = ''
                    for i, (new_cid, status) in enumerate(new_cids):
                        new_commit_comment += (new_cid + ' : ' + status)
                        if i+1 != len(new_cids):
                            new_commit_comment += '\n'

                        if status != 'ABANDONED':
                            if new_status == 'Merge':
                                pass
                            else:
                                if status == 'MERGED':
                                    new_status = 'Merge'
                                else:
                                    new_status = 'Upload'

                    #print('cherrypick_cid: ', cherrypick_cid)
                    #print('new_commit_comment: ', new_commit_comment)
                    #print('new_status: ', new_status)
                    #print('')
                    if new_status:
                        ws_src.cell(row_index + 1, status_column_index + 1).value = new_status
                        if new_status == 'Merge':
                            ws_src.cell(row_index + 1, cherrypick_column_index + 1).value = 'Y'
                    if new_commit_comment:
                        ws_src.cell(row_index + 1, new_commitId_column_index + 1).value = new_commit_comment
            # CherryPick N == NotNeed
            for i in range(len(commitId_src_list)):
                if ws_src.cell(i + 1, cherrypick_column_index + 1).value == 'N':
                    ws_src.cell(i + 1, status_column_index + 1).value = 'NotNeed'

        wb_src.save(dest_doc)
