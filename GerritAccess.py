import re, time, requests
import json
from bs4 import BeautifulSoup
from os import path
import pickle

class GerritAccess:
    USERNAME = ''
    PASSWORD = ''
    GERRIT_URL = ''
    GERRIT_COMMIT_ID_FILENAME = 'gerrit_commit_id_dict.pkl'
    
    def __init__(self, url, username, password):
        self.URL = url
        self.USERNAME = username
        self.PASSWORD = password

    def save_ojb(self, obj, name):
        with open(name, 'wb') as f:
            pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

    def load_obj(self, name):
        if not path.exists(name):
            return {}
        with open(name, 'rb') as f:
            return pickle.load(f)

    def get_cherrypick_gerrit_ids(self, branch):
        if not self.USERNAME or not self.PASSWORD:
            return None

        QUERY_CHANGES = '/a/changes/'
        QUERY_CHANGE_OPTION = '?q=branch:' + branch + '&o=CURRENT_COMMIT&o=CURRENT_REVISION'
        QUERY_INDEX_OPTION = '&start='

        index = 0
        cherrypick_gerrit_id_dict = {}
        while True:
            result = requests.get(self.URL + QUERY_CHANGES + QUERY_CHANGE_OPTION + QUERY_INDEX_OPTION + str(index), auth=(self.USERNAME, self.PASSWORD))
            result.encoding = 'utf-8'

            if result.status_code == 200:
                pass
            else:
                print(result.status_code, result.text)
                return None

            #Remove first line (Don't know why)
            first_line_index = result.text.find('\n')
            result = result.text[first_line_index+1:]

            response = json.loads(result)
            print('Query length = ', len(response))
            index += len(response)

            more_changes = False
            for commit in response:
                status = commit['status']
                current_commit_id = commit['current_revision']
                message = commit['revisions'][current_commit_id]['commit']['message']
                if '_more_changes' in commit:
                    more_changes = commit['_more_changes']

                for line in message.splitlines():
                    if 'spdggit.eww.panasonic.com' in line.lower():
                        matchObj = re.match( r'.+spdggit.eww.panasonic.com[^0-9]+([0-9]+)/?', line, re.M|re.I)
                        if matchObj is not None:
                            cherrypick_id = matchObj[1]
                            if not cherrypick_id in cherrypick_gerrit_id_dict:
                                cherrypick_gerrit_id_dict[cherrypick_id] = []
                            cherrypick_gerrit_id_dict[cherrypick_id].append((current_commit_id, status))

            if more_changes == False:
                break

        print('Query len = ', len(cherrypick_gerrit_id_dict))
        return cherrypick_gerrit_id_dict

    def get_cherrypick_commit_ids_by_gids(self, cherrypick_gerrit_id_dict):
        if not self.USERNAME or not self.PASSWORD:
            return None

        QUERY_CHANGES = '/a/changes/'
        QUERY_CHANGE_OPTION = '?o=CURRENT_REVISION'

        gerrit_commit_id_dict = self.load_obj(self.GERRIT_COMMIT_ID_FILENAME)
        print('stored gerrit_commit_id len = ', len(gerrit_commit_id_dict))
        if gerrit_commit_id_dict == None:
            gerrit_commit_id_dict = {}

        cherrypick_commit_id_dict = {}
        for i, (cherrypick_gerrit_id, now_commit_ids) in enumerate(cherrypick_gerrit_id_dict.items()):
            cherrypick_commit_id = ''
            if cherrypick_gerrit_id in gerrit_commit_id_dict:
                cherrypick_commit_id = gerrit_commit_id_dict[cherrypick_gerrit_id]
            else:
                #print('cherrypick_gerrit_id: ', cherrypick_gerrit_id)
                result = requests.get(self.URL + QUERY_CHANGES + str(cherrypick_gerrit_id) + QUERY_CHANGE_OPTION, auth=(self.USERNAME, self.PASSWORD))
                result.encoding = 'utf-8'
                #print(result.status_code)
                #print(result.text)

                #Remove first line (Don't know why)
                first_line_index = result.text.find('\n')
                result = result.text[first_line_index+1:]

                response = json.loads(result)
                cherrypick_commit_id = response['current_revision']
                gerrit_commit_id_dict[cherrypick_gerrit_id] = cherrypick_commit_id

            cherrypick_commit_id_dict[cherrypick_commit_id] = now_commit_ids
            print(str(i+1) + '/' + str(len(cherrypick_gerrit_id_dict)))

        self.save_ojb(gerrit_commit_id_dict, self.GERRIT_COMMIT_ID_FILENAME)

        return cherrypick_commit_id_dict
